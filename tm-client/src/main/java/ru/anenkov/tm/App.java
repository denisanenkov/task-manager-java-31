package ru.anenkov.tm;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import ru.anenkov.tm.bootstrap.BootstrapClient;
import ru.anenkov.tm.config.AppConfiguration;

public class App {

    @SneakyThrows
    public static void main(@Nullable final String[] args) {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(AppConfiguration.class);
        BootstrapClient bootstrapClient = context.getBean(BootstrapClient.class);
        bootstrapClient.run(args);
    }

}
