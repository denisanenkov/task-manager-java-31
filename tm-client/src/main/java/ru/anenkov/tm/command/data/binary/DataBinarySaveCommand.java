package ru.anenkov.tm.command.data.binary;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.command.AbstractCommandClient;
import ru.anenkov.tm.endpoint.AdminEndpoint;
import ru.anenkov.tm.enumeration.Role;

import java.io.IOException;

@Component
public class DataBinarySaveCommand extends AbstractCommandClient {

    @NotNull
    @Autowired
    private AdminEndpoint adminEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "Data-bin-save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("[DATA BINARY SAVE]");
        adminEndpoint.saveDataBinary(bootstrap.getSession());
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
