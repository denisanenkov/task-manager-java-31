package ru.anenkov.tm.command.system;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.command.AbstractCommandClient;

@Component
public class VersionClientCommand extends AbstractCommandClient {

    @Override
    public @Nullable String arg() {
        return "-v";
    }

    @Override
    public @Nullable String name() {
        return "Version";
    }

    @Override
    public @Nullable String description() {
        return "Show version app";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[VERSION APP]");
        System.out.println("\t1.2.2");
        System.out.println("  [SUCCESS]");
    }

}
