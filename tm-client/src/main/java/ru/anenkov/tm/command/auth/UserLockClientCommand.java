package ru.anenkov.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.command.AbstractCommandClient;
import ru.anenkov.tm.endpoint.AdminUserEndpoint;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.TerminalUtil;

import java.util.Locale;

@Component
public class UserLockClientCommand extends AbstractCommandClient {

    @NotNull
    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String name() {
        return "Lock-user";
    }

    @Override
    public @Nullable String description() {
        return "Lock user (admin command)";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOCK USER]");
        System.out.print("ENTER LOGIN: ");
        @NotNull final String login = TerminalUtil.nextLine();
        adminUserEndpoint.lockUserByLogin(bootstrap.getSession(), login);
        System.out.println("[LOCK USER WITH LOGIN \"" + login.toUpperCase(Locale.ROOT) + "\" SUCCESS]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
