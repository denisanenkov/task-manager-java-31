package ru.anenkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.command.AbstractCommandClient;
import ru.anenkov.tm.endpoint.TaskEndpoint;
import ru.anenkov.tm.endpoint.User;
import ru.anenkov.tm.endpoint.UserDTO;
import ru.anenkov.tm.endpoint.UserEndpoint;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.TerminalUtil;

import java.util.List;

@Component
public class ProfileClientCommand extends AbstractCommandClient {

    @NotNull
    @Autowired
    private UserEndpoint userEndpoint;

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String name() {
        return "Profile";
    }

    @Override
    public @Nullable String description() {
        return "Show user profile";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER PROFILE]");
        System.out.print("ENTER LOGIN OF USER: ");
        String login = TerminalUtil.nextLine();
        UserDTO user = userEndpoint.findByLoginUser(bootstrap.getSession(), login);
        System.out.println("LOGIN: " + user.getLogin() +
                ", \nFIRST NAME: " + user.getFirstName() +
                ", \nMIDDLE NAME: " + user.getMiddleName() +
                ", \nLAST NAME: " + user.getLastName() +
                ", \nEMAIL: " + user.getEmail());
        System.out.println("[SUCCESS]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}
