package ru.anenkov.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.command.AbstractCommandClient;
import ru.anenkov.tm.endpoint.Session;
import ru.anenkov.tm.endpoint.SessionDTO;
import ru.anenkov.tm.endpoint.SessionEndpoint;
import ru.anenkov.tm.util.TerminalUtil;

@Component
public class LoginClientCommand extends AbstractCommandClient {

    @NotNull
    @Autowired
    private SessionEndpoint sessionEndpoint;

    @Override
    public @Nullable String arg() {
        return "-log";
    }

    @Override
    public @Nullable String name() {
        return "Login";
    }

    @Override
    public @Nullable String description() {
        return "Login user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOGIN]");
        System.out.print("ENTER LOGIN: ");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.print("ENTER PASSWORD: ");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final SessionDTO session = sessionEndpoint.openSession(login, password);
        bootstrap.setSession(session);
        if (bootstrap.getSession() != null) System.out.println("[LOGIN SUCCESS]");
    }

}
