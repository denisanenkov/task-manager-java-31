package ru.anenkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.command.AbstractCommandClient;
import ru.anenkov.tm.endpoint.ProjectEndpoint;
import ru.anenkov.tm.endpoint.TaskEndpoint;
import ru.anenkov.tm.enumeration.Role;

@Component
public class TaskClearClientCommand extends AbstractCommandClient {

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String name() {
        return "Task-clear";
    }

    @Override
    public @Nullable String description() {
        return "Clear tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CLEAR TASK]");
        taskEndpoint.clear(bootstrap.getSession());
        System.out.println("[CLEAR SUCCESS]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}
