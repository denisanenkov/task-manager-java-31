package ru.anenkov.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.command.AbstractCommandClient;
import ru.anenkov.tm.endpoint.AdminUserEndpoint;
import ru.anenkov.tm.endpoint.UserEndpoint;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.TerminalUtil;

@Component
public class UserDeleteClientCommand extends AbstractCommandClient {

    @NotNull
    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "Delete-user";
    }

    @NotNull
    @Override
    public String description() {
        return "Delete user";
    }

    @Override
    public void execute() {
        System.out.println("[DELETE USER]");
        System.out.print("ENTER LOGIN: ");
        @NotNull final String login = TerminalUtil.nextLine();
        adminUserEndpoint.deleteUserByLogin(bootstrap.getSession(), login);
        System.out.println("[DELETE SUCCESS]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
