package ru.anenkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.command.AbstractCommandClient;
import ru.anenkov.tm.endpoint.UserEndpoint;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.TerminalUtil;

@Component
public class UserUpdateEmailClientCommand extends AbstractCommandClient {

    @NotNull
    @Autowired
    private UserEndpoint userEndpoint;

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String name() {
        return "Update-email";
    }

    @Override
    public @Nullable String description() {
        return "Update User Email";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE USER MAIL]");
        System.out.print("ENTER NEW USER MAIL: ");
        @NotNull final String newUserEmail = TerminalUtil.nextLine();
        userEndpoint.updateUserEmail(bootstrap.getSession(), newUserEmail);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
