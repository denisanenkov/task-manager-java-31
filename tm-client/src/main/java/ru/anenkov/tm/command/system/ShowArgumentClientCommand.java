package ru.anenkov.tm.command.system;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.command.AbstractCommandClient;

import java.util.Collection;

@Component
public class ShowArgumentClientCommand extends AbstractCommandClient {

    @Override
    public @Nullable String arg() {
        return "-a";
    }

    @Override
    public @Nullable String name() {
        return "Arguments";
    }

    @Override
    public @Nullable String description() {
        return "View arguments";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[ARGUMENTS]");
        @Nullable Collection<AbstractCommandClient> commandClients = bootstrap.viewCommands();
        int index = 0;
        for (@Nullable AbstractCommandClient abstractCommandClient : commandClients) {
            String argument = abstractCommandClient.arg();
            if (argument != null) {
                index++;
                System.out.println(index + ".\t" + abstractCommandClient.arg());
            }
        }
        if (index == 0) System.out.println("[ARGUMENT LIST IS EMPTY]");
        System.out.println("[SUCCESS]");
    }

}
