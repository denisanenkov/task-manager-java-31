package ru.anenkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.command.AbstractCommandClient;
import ru.anenkov.tm.endpoint.TaskEndpoint;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.TerminalUtil;

@Component
public class TaskUpdateByIdClientCommand extends AbstractCommandClient {

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String name() {
        return "Task-update-by-id";
    }

    @Override
    public @Nullable String description() {
        return "Update task - list by id";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE TASK]");
        System.out.print("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.print("ENTER NEW NAME TASK: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.print("ENTER NEW DESCRIPTION TASK: ");
        @NotNull final String description = TerminalUtil.nextLine();
        taskEndpoint.updateTaskById(bootstrap.getSession(), id, name, description);
        System.out.println("[UPDATE SUCCESS]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}

