package ru.anenkov.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.command.AbstractCommandClient;
import ru.anenkov.tm.endpoint.SessionEndpoint;
import ru.anenkov.tm.enumeration.Role;

import java.sql.SQLOutput;

@Component
public class LogoutClientCommand extends AbstractCommandClient {

    @NotNull
    @Autowired
    private SessionEndpoint sessionEndpoint;

    @Override
    public @Nullable String arg() {
        return "-lout";
    }

    @Override
    public @Nullable String name() {
        return "Logout";
    }

    @Override
    public @Nullable String description() {
        return "Logout user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOGOUT]");
        sessionEndpoint.closeSession(bootstrap.getSession());
        bootstrap.clearSession();
        System.out.println("[LOGOUT SUCCESS]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
