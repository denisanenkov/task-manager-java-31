package ru.anenkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.command.AbstractCommandClient;
import ru.anenkov.tm.endpoint.Project;
import ru.anenkov.tm.endpoint.ProjectDTO;
import ru.anenkov.tm.endpoint.ProjectEndpoint;
import ru.anenkov.tm.endpoint.Task;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.exception.empty.EmptyListException;

import java.util.ArrayList;
import java.util.List;

@Component
public class ProjectListShowClientCommand extends AbstractCommandClient {

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String name() {
        return "Project-list";
    }

    @Override
    public @Nullable String description() {
        return "Get Project list";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT LIST]\n");
        @Nullable List<ProjectDTO> projectList = new ArrayList<>();
        int count = 1;
        try {
            projectList = projectEndpoint.getListProjects(bootstrap.getSession());
        } catch (EmptyListException ex) {
            ex.getMessage();
            System.out.println("[PROJECT LIST IS EMPTY]");
        }
        for (ProjectDTO project : projectList) {
            System.out.println("Number of project: " + (count++) + "\nName project: " + project.getName() +
                    "\nDescription project: " + project.getDescription() + "\n");
        }
        System.out.println("[SUCCESS]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}

