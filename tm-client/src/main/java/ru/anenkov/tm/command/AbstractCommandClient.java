package ru.anenkov.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.bootstrap.BootstrapClient;
import ru.anenkov.tm.config.AppConfiguration;
import ru.anenkov.tm.enumeration.Role;

@Component
public abstract class AbstractCommandClient {

    @Autowired
    protected BootstrapClient bootstrap;

    @Nullable
    public Role[] roles() {
        return null;
    }

    @Nullable
    public abstract String arg();

    @Nullable
    public abstract String name();

    @Nullable
    public abstract String description();

    public abstract void execute() throws Exception;

}
