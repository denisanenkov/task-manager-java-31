package ru.anenkov.tm.command.system;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.command.AbstractCommandClient;

import java.util.Collection;

@Component
public class HelpClientCommand extends AbstractCommandClient {

    @Override
    public @Nullable String arg() {
        return "-h";
    }

    @Override
    public @Nullable String name() {
        return "Help";
    }

    @Override
    public @Nullable String description() {
        return "View commands";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[HELP]");
        Collection<AbstractCommandClient> commandClients = bootstrap.viewCommands();
        int index = 1;
        for (AbstractCommandClient abstractCommandClient : commandClients) {
            System.out.println(index + ".\t" + abstractCommandClient.name()
                    + " --> " + abstractCommandClient.description());
            index++;
        }
        System.out.println("[SUCCESS]");
    }

}
