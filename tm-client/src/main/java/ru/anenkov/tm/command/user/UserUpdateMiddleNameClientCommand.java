package ru.anenkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.command.AbstractCommandClient;
import ru.anenkov.tm.endpoint.UserEndpoint;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.TerminalUtil;

@Component
public class UserUpdateMiddleNameClientCommand extends AbstractCommandClient {

    @NotNull
    @Autowired
    private UserEndpoint userEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "Update-middle-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Update middle name";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE USER MIDDLE NAME]");
        System.out.print("ENTER NEW USER MIDDLE NAME: ");
        @NotNull final String newSecondName = TerminalUtil.nextLine();
        userEndpoint.updateUserMiddleName(bootstrap.getSession(), newSecondName);
        System.out.println("[NAME UPDATE SUCCESS]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
