package ru.anenkov.tm.bootstrap;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.command.AbstractCommandClient;
import ru.anenkov.tm.constant.MessageConst;
import ru.anenkov.tm.endpoint.*;
import ru.anenkov.tm.exception.system.IncorrectCommandException;
import ru.anenkov.tm.util.TerminalUtil;

import java.util.*;

@Getter
@Setter
@Component
@Scope("singleton")
public class BootstrapClient {

    @Nullable
    SessionDTO session = null;

    public void clearSession() {
        session = null;
    }

    @NotNull
    @Autowired
    public AbstractCommandClient[] commandClientList;

    @NotNull
    private final Map<String, AbstractCommandClient> commands = new LinkedHashMap<>();


    private void initCommands(@NotNull final AbstractCommandClient[] abstractCommandClients) {
        for (@NotNull final AbstractCommandClient abstractCommandClient : abstractCommandClients) {
            commands.put(abstractCommandClient.name(), abstractCommandClient);
        }
    }

    public void run(@Nullable final String[] args) throws Exception {
        initCommands(commandClientList);
        System.out.println(MessageConst.WELCOME);
        if (parseArgs(args)) System.exit(0);
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.out.println("[FAIL]");
            }
        }
    }

    @Nullable
    public Collection<AbstractCommandClient> viewCommands() {
        return commands.values();
    }

    public boolean parseArgs(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        return true;
    }

    @SneakyThrows
    private void parseCommand(@Nullable final String cmd) {
        if (cmd == null || cmd.isEmpty()) return;
        @Nullable final AbstractCommandClient command = commands.get(cmd);
        if (command == null) throw new IncorrectCommandException(cmd);
        command.execute();
    }

}
