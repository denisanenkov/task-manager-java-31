import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.anenkov.tm.endpoint.*;
import ru.anenkov.tm.marker.RemoveCategory;
import ru.anenkov.tm.marker.AllCategory;
import ru.anenkov.tm.marker.UpdateCategory;

@Category(AllCategory.class)
public class AdminUserEndpointTest {

    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    private final AdminUserEndpointService adminUserEndpointService = new AdminUserEndpointService();
    private final ru.anenkov.tm.endpoint.AdminUserEndpoint adminUserEndpoint = adminUserEndpointService.getAdminUserEndpointPort();

    @Test
    @Category(RemoveCategory.class)
    public void deleteUser() {
        final UserEndpointService userEndpointService = new UserEndpointService();
        final UserEndpoint userEndpoint = userEndpointService.getUserEndpointPort();
        final SessionDTO session = sessionEndpoint.openSession("admin", "admin");
        userEndpoint.createUser(session, "first", "first");
        userEndpoint.createUser(session, "second", "second");
        //user2.setEmail("den@mail.ru");
        userEndpoint.createUser(session, "third", "third");
        Integer count = userEndpoint.findAllUser(session).size();
        adminUserEndpoint.removeByLoginUser(session, "first");
        Assert.assertEquals(count - 1, userEndpoint.findAllUser(session).size());
        adminUserEndpoint.removeByLoginUser(session, "second");
        Assert.assertEquals(count - 2, userEndpoint.findAllUser(session).size());
        adminUserEndpoint.removeByIdUser(session, userEndpoint.findByLoginUser(session, "third").getId());
        Assert.assertEquals(count - 3, userEndpoint.findAllUser(session).size());
    }

    @Test
    @Category(UpdateCategory.class)
    public void lockUserTest() {
        final UserEndpointService userEndpointService = new UserEndpointService();
        final UserEndpoint userEndpoint = userEndpointService.getUserEndpointPort();
        final SessionDTO session = sessionEndpoint.openSession("admin", "admin");
        int count = userEndpoint.findAllUser(session).size();
        userEndpoint.createUser(session, "first", "first");
        Assert.assertEquals(count + 1, userEndpoint.findAllUser(session).size());
        Assert.assertEquals(false, userEndpoint.findByLoginUser(session, "first").isLocked());
        adminUserEndpoint.lockUserByLogin(session, "first");
        Assert.assertEquals(true, userEndpoint.findByLoginUser(session, "first").isLocked());
        adminUserEndpoint.removeByLoginUser(session, "first");
        Assert.assertEquals(count, userEndpoint.findAllUser(session).size());
    }

    @Test
    @Category(UpdateCategory.class)
    public void unlockUserTest() {
        final UserEndpointService userEndpointService = new UserEndpointService();
        final UserEndpoint userEndpoint = userEndpointService.getUserEndpointPort();
        final SessionDTO session = sessionEndpoint.openSession("admin", "admin");
        int count = userEndpoint.findAllUser(session).size();
        userEndpoint.createUser(session, "first", "first");
        userEndpoint.findByLoginUser(session, "first");
        Assert.assertEquals(count + 1, userEndpoint.findAllUser(session).size());
        Assert.assertEquals(false, userEndpoint.findByLoginUser(session, "first").isLocked());
        adminUserEndpoint.lockUserByLogin(session, "first");
        Assert.assertEquals(true, userEndpoint.findByLoginUser(session, "first").isLocked());
        adminUserEndpoint.unlockUserByLogin(session, "first");
        Assert.assertEquals(false, userEndpoint.findByLoginUser(session, "first").isLocked());
        adminUserEndpoint.removeByLoginUser(session, "first");
        Assert.assertEquals(count, userEndpoint.findAllUser(session).size());
    }

}
