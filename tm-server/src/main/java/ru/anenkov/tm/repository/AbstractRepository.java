package ru.anenkov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.anenkov.tm.api.repository.IRepository;
import ru.anenkov.tm.dto.entitiesDTO.AbstractEntityDTO;
import ru.anenkov.tm.entity.AbstractEntity;
import ru.anenkov.tm.exception.empty.EmptyManagerException;
import ru.anenkov.tm.exception.system.IncorrectDataException;
import ru.anenkov.tm.util.EntityManagerUtil;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public abstract class AbstractRepository<T extends AbstractEntity, D extends AbstractEntityDTO> implements IRepository<T> {

    @Nullable
    @Autowired
    protected EntityManager entityManager;

    public List<T> merge(@Nullable final Collection<T> t) {
        if (t == null || t.isEmpty()) return null;
        @NotNull List<T> entities = new ArrayList<>();
        for (@NotNull final T entity : t) {
            assert entityManager != null;
            entityManager.merge(entity);
            entities.add(entity);
        }
        return entities;
    }

    public T merge(@Nullable final T t) {
        if (t == null) return null;
        assert entityManager != null;
        entityManager.merge(t);
        return t;
    }

    public List<T> merge(@Nullable final T... t) {
        if (t == null || t.length == 0) return null;
        @NotNull List<T> entities = new ArrayList<>();
        for (@Nullable final T entity : t) {
            entityManager.merge(entity);
            entities.add(entity);
        }
        return entities;
    }

    public void load(@Nullable final Collection<T> t) {
        clear();
        entityManager.merge(t);
    }

    public void load(@Nullable final T... t) {
        clear();
        entityManager.merge(t);
    }

    public void load(@Nullable final T t) {
        clear();
        entityManager.merge(t);
    }

    public void clear() {
        entityManager.clear();
    }

    public void removeEntity(@NotNull T entity) {
        entityManager.remove(entity);
    }

    @NotNull
    public List<T> findAll() {
        return getList();
    }

    @SneakyThrows
    public T getFirstEntity(@Nullable final List<T> tList) {
        if (tList.isEmpty() || tList.size() == 0) throw new IncorrectDataException();
        return tList.get(0);
    }

    @SneakyThrows
    public D getFirstDTO(@Nullable final List<D> dList) {
        if (dList.isEmpty() || dList.size() == 0) throw new IncorrectDataException();
        return dList.get(0);
    }

    public void remove(@NotNull T entity) {
        entityManager.remove(entity);
    }

    public void startTransaction() {
        if (entityManager != null) {
            entityManager.getTransaction().begin();
        } else throw new EmptyManagerException();
    }

    public void commitTransaction() {
        if (entityManager != null) {
            entityManager.getTransaction().commit();
        } else throw new EmptyManagerException();
    }

    public void closeTransaction() {
        if (entityManager != null) {
            entityManager.close();
        } else throw new EmptyManagerException();
    }

    public void rollbackTransaction() {
        if (entityManager != null) {
            entityManager.getTransaction().rollback();
        } else throw new EmptyManagerException();
    }

}
