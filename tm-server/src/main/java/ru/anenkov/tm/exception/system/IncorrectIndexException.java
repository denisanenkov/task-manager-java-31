package ru.anenkov.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.anenkov.tm.exception.AbstractException;

public class IncorrectIndexException extends AbstractException {

    public IncorrectIndexException(@NotNull Throwable cause) {
        super(cause);
    }

    public IncorrectIndexException(@NotNull String value) {
        super("Error! This value ``" + value + "`` is not number... ");
    }

    public IncorrectIndexException() {
        super("Error! Index is incorrect!");
    }

}