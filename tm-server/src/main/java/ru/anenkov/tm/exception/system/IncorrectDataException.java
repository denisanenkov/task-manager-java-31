package ru.anenkov.tm.exception.system;

public class IncorrectDataException extends ArithmeticException {

    public IncorrectDataException() {
        System.out.println("Error! Incorrect data entered! Command failed.. ");
    }

}
