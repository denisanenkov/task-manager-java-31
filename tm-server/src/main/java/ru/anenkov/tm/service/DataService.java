package ru.anenkov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.anenkov.tm.api.service.IDataService;
import ru.anenkov.tm.api.service.IDomainService;
import ru.anenkov.tm.constant.DataConst;
import ru.anenkov.tm.dto.entitiesDTO.Domain;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

@Service
public final class DataService implements IDataService {

    @Nullable
    @Autowired
    private IDomainService domainService;

    @Override
    @SneakyThrows
    public void saveDataBinary() {
        @Nullable final Domain domain = returnDomain();
        @Nullable final File file = returnFile(DataConst.FILE_BINARY);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void saveDataBase64() {
        @Nullable final Domain domain = returnDomain();
        @Nullable final File file = returnFile(DataConst.FILE_BASE64);
        @NotNull final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        objectOutputStream.writeObject(domain);
        final byte[] bytes = byteArrayOutputStream.toByteArray();
        @NotNull final String base64 = new BASE64Encoder().encode(bytes);
        fileOutputStream.write(base64.getBytes());
        fileOutputStream.flush();
        objectOutputStream.close();
        byteArrayOutputStream.close();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void saveDataXML() {
        @Nullable final Domain domain = returnDomain();
        @Nullable final File file = returnFile(DataConst.FILE_XML);
        @NotNull final XmlMapper objectMapper = new XmlMapper();
        @NotNull final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(xml.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void saveDataJson() {
        @Nullable final Domain domain = returnDomain();
        @Nullable final File file = returnFile(DataConst.FILE_JSON);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void loadDataBinary() {
        @NotNull final FileInputStream fileInputStream = new FileInputStream(DataConst.FILE_BINARY);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        domainService.load(domain);
        objectInputStream.close();
        fileInputStream.close();
    }

    @Override
    @SneakyThrows
    public void loadDataBase64() {
        @NotNull final String base64date = new String(Files.readAllBytes(Paths.get(DataConst.FILE_BASE64)));
        final byte[] decodedData = new BASE64Decoder().decodeBuffer(base64date);
        @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(decodedData);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        domainService.load(domain);
        objectInputStream.close();
        byteArrayInputStream.close();
    }

    @Override
    @SneakyThrows
    public void loadDataXML() {
        @NotNull final FileInputStream fileInputStream = new FileInputStream(DataConst.FILE_XML);
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final Domain domain = objectMapper.readValue(fileInputStream, Domain.class);
        domainService.load(domain);
        fileInputStream.close();
    }

    @Override
    @SneakyThrows
    public void loadDataJson() {
        @Nullable final String path = DataConst.FILE_JSON;
        if (path == null) return;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = new String(Files.readAllBytes(Paths.get(path)));
        @NotNull Domain domain = objectMapper.readValue(json, Domain.class);
        domainService.load(domain);
    }

    @Override
    @SneakyThrows
    public void clearDataBinary() {
        clearFile(DataConst.FILE_BINARY);
    }

    @Override
    @SneakyThrows
    public void clearDataBase64() {
        clearFile(DataConst.FILE_BASE64);
    }

    @Override
    @SneakyThrows
    public void clearDataXML() {
        clearFile(DataConst.FILE_XML);
    }

    @Override
    @SneakyThrows
    public void clearDataJson() {
        clearFile(DataConst.FILE_JSON);
    }

    @Override
    public Domain returnDomain() {
        @Nullable final Domain domain = new Domain();
        domainService.export(domain);
        return domain;
    }

    @Override
    @SneakyThrows
    public File returnFile(@Nullable final String nameFile) {
        @Nullable final File file = new File(nameFile);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        return file;
    }

    @Override
    @SneakyThrows
    public void clearFile(@Nullable final String pathFile) {
        @NotNull final File file = new File(pathFile);
        Files.deleteIfExists(file.toPath());
    }

}
