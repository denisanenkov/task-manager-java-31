package ru.anenkov.tm.dto.entitiesDTO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.entity.Project;
import ru.anenkov.tm.entity.Task;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public final class TaskDTO extends AbstractEntityDTO implements Serializable {

    private static final long serialVersionUID = 1001L;

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @Nullable
    private String userId = "";

    @Nullable
    private String projectId;


    @Override
    public String toString() {
        return "\nTask:\n" +
                "\nname = '" + name + '\'' +
                ", \ndescription ='" + description + '\'' +
                ", \nuserId ='" + userId + '\'' +
                "\n";
    }

    public TaskDTO(
            @NotNull final String userId,
            @NotNull final String name
    ) {
        this.userId = userId;
        this.name = name;
    }

    public TaskDTO(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) {
        this.userId = userId;
        this.name = name;
        this.description = description;
    }


    public TaskDTO(@Nullable final Task task) {
        if (task == null) return;
        assert task.getName() != null;
        name = task.getName();
        assert task.getDescription() != null;
        description = task.getDescription();
        if (task.getUser() != null) userId = task.getUser().getId();
    }

    @Nullable
    public static TaskDTO toDTO(@Nullable final Task task) {
        if (task == null) return null;
        return new TaskDTO(task);
    }

    @NotNull
    public static List<TaskDTO> toDTO(@Nullable final Collection<Task> tasks) {
        if (tasks == null || tasks.isEmpty()) return Collections.emptyList();
        @org.jetbrains.annotations.NotNull final List<TaskDTO> result = new ArrayList<>();
        for (@Nullable final Task task : tasks) {
            if (task == null) continue;
            result.add(new TaskDTO(task));
        }
        return result;
    }

}