package ru.anenkov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties
@Entity
@Table(name = "app_session")
public class Session extends AbstractEntity implements Cloneable {

    @Override
    public Session clone() {
        try {
            return (Session) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    public static final long serialVersionUID = 1L;

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();

    @ManyToOne
    @Nullable
    User user;

    @Nullable
    private Long timestamp;

    @Nullable
    private String signature;

    public Session(User user) {
        setUser(user);
    }

    @Override
    public String toString() {
        return "\nSession:\n" +
                "\ntimestamp =" + timestamp +
                ", \nsignature ='" + signature + '\'' +
                "\n";
    }

}

