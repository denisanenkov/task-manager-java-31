package ru.anenkov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.dto.entitiesDTO.Domain;

public interface IDomainService {

    void load(@Nullable Domain domain);

    void export(@Nullable Domain domain);

}
