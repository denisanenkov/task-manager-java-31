package ru.anenkov.tm.config;

import org.springframework.context.annotation.*;
import ru.anenkov.tm.util.EntityManagerUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.io.IOException;

@Configuration
@ComponentScan("ru.anenkov.tm")
public class ApplicationConfiguration {

    @Bean
    @Scope("singleton")
    public EntityManagerFactory entityManagerFactory() throws IOException {
        return EntityManagerUtil.factory();
    }

    @Bean
    @Scope("prototype")
    public EntityManager entityManager(final EntityManagerFactory entityManagerFactory) {
        return entityManagerFactory.createEntityManager();
    }

}
