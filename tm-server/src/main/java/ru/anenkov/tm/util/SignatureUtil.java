package ru.anenkov.tm.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.dto.entitiesDTO.SessionDTO;
import ru.anenkov.tm.entity.Session;
import ru.anenkov.tm.service.SessionService;
import ru.anenkov.tm.service.UserService;

import java.util.UUID;

public final class SignatureUtil {

    @Nullable
    public static String sign(
            @NotNull final Object value,
            @NotNull final String salt,
            @NotNull final Integer cycle
    ) {
        try {
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @Nullable final String json = objectMapper.writeValueAsString(value);
            return sign(json, salt, cycle);
        } catch (JsonProcessingException e) {
            return e.getMessage();
        }
    }

    @Nullable
    public static String sign(
            @Nullable final String value,
            @Nullable final String salt,
            @NotNull final Integer cycle
    ) {
        if (value == null || salt == null) return null;
        @Nullable String result = value;
        for (int i = 0; i < cycle; i++) {
            result = HashUtil.MD5(salt + result + salt);
        }
        return result;
    }

}
