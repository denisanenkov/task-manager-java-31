package ru.anenkov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.anenkov.tm.api.endpoint.IAdminEndpoint;
import ru.anenkov.tm.dto.entitiesDTO.SessionDTO;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.service.DataService;
import ru.anenkov.tm.service.SessionService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@Controller
public class AdminEndpoint implements IAdminEndpoint {

    @Nullable
    @Autowired
    private SessionService sessionService;

    @Nullable
    @Autowired
    private DataService dataService;

    @SneakyThrows
    @WebMethod
    public void saveDataBinary(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) {
        sessionService.validate(sessionService.toSession(session), Role.ADMIN);
        dataService.saveDataBinary();

    }

    @SneakyThrows
    @WebMethod
    public void loadDataBinary(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) {
        sessionService.validate(sessionService.toSession(session), Role.ADMIN);
        dataService.loadDataBinary();
    }

    @SneakyThrows
    @WebMethod
    public void clearDataBinary(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) {
        sessionService.validate(sessionService.toSession(session), Role.ADMIN);
        dataService.clearDataBinary();
    }

    @SneakyThrows
    @WebMethod
    public void saveDataBase64(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) {
        sessionService.validate(sessionService.toSession(session), Role.ADMIN);
        dataService.saveDataBase64();
    }

    @SneakyThrows
    @WebMethod
    public void loadDataBase64(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) {
        sessionService.validate(sessionService.toSession(session), Role.ADMIN);
        dataService.loadDataBase64();
    }

    @SneakyThrows
    @WebMethod
    public void clearDataBase64(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) {
        sessionService.validate(sessionService.toSession(session), Role.ADMIN);
        dataService.clearDataBase64();
    }

    @SneakyThrows
    @WebMethod
    public void saveDataXml(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) {
        sessionService.validate(sessionService.toSession(session), Role.ADMIN);
        dataService.saveDataXML();
    }

    @SneakyThrows
    @WebMethod
    public void loadDataXml(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) {
        sessionService.validate(sessionService.toSession(session), Role.ADMIN);
        dataService.loadDataXML();
    }

    @SneakyThrows
    @WebMethod
    public void clearDataXml(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) {
        sessionService.validate(sessionService.toSession(session), Role.ADMIN);
        dataService.clearDataXML();
    }

    @SneakyThrows
    @WebMethod
    public void saveDataJson(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) {
        sessionService.validate(sessionService.toSession(session), Role.ADMIN);
        dataService.saveDataJson();
    }

    @SneakyThrows
    @WebMethod
    public void loadDataJson(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) {
        sessionService.validate(sessionService.toSession(session), Role.ADMIN);
        dataService.loadDataJson();
    }

    @SneakyThrows
    @WebMethod
    public void cleanDataJson(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) {
        sessionService.validate(sessionService.toSession(session), Role.ADMIN);
        dataService.clearDataJson();
    }

}
