package ru.anenkov.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.api.repository.*;
import ru.anenkov.tm.api.service.*;
import ru.anenkov.tm.constant.DataConst;
import ru.anenkov.tm.endpoint.*;
import ru.anenkov.tm.repository.*;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.service.*;
import ru.anenkov.tm.util.EntityManagerUtil;

import javax.xml.ws.Endpoint;
import java.io.IOException;

@Component
public final class Bootstrap {

    @Autowired
    UserService userService;

    @Autowired
    ApplicationContext applicationContext;

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    private SessionEndpoint sessionEndpoint;

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private UserEndpoint userEndpoint;

    @NotNull
    @Autowired
    private AdminEndpoint adminEndpoint;

    @NotNull
    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    @NotNull
    @Autowired
    private CalcEndpoint calcEndpoint;

    public Bootstrap() throws IOException {
    }

    private void initEndpoint() {
        registryEndpoint(adminEndpoint);
        registryEndpoint(sessionEndpoint);
        registryEndpoint(userEndpoint);
        registryEndpoint(projectEndpoint);
        registryEndpoint(taskEndpoint);
        registryEndpoint(adminUserEndpoint);
        registryEndpoint(calcEndpoint);
    }

    private void registryEndpoint(final Object endpoint) {
        if (endpoint == null) return;
        final String host = DataConst.SERVER_HOST;
        final String port = DataConst.SERVER_PORT;
        final String name = endpoint.getClass().getSimpleName();
        final String wsdl = "http://" + host + ":" + port + "/" + name + "?WSDL";
        Endpoint.publish(wsdl, endpoint);
        System.out.println(wsdl);
    }

    public void initUsers() {
        userService.create("1", "1", "test@mail.ru");
        userService.create("test", "test", "test@mail.ru");
        userService.create("admin", "admin", Role.ADMIN);
        userService.create("2", "2", "den@mail.ru");
    }

    public void run(@Nullable final String[] args) throws Exception {
        initEndpoint();
        //initUsers();
        System.out.println("\t\t***SERVERS*STARTED***");
        if (parseArgs(args)) System.exit(0);
    }

    public boolean parseArgs(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        return true;
    }

}